import 'package:betgogo/Home/creatform.dart';
import 'package:betgogo/appbar/c1.dart';
import 'package:betgogo/appbar/c2.dart';
import 'package:flutter/material.dart';

import '../Home/actualbackcreatebet.dart';
import '../Home/createbet.dart';
class creates_bets extends StatefulWidget {
  const creates_bets({Key? key}) : super(key: key);

  @override
  State<creates_bets> createState() => _creates_betsState();
}

class _creates_betsState extends State<creates_bets>
    with SingleTickerProviderStateMixin{

  late TabController _tabController;
  var pagesAll = [c1(), c2()];
  int _selectedIndex = 0;
  @override
  void initState() {
    _tabController = new TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(

        body: actualbackcbet(
        child: Container(
        width: size.width,


        child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
        SizedBox(height: size.height/10),
    Text('Create your bet here ! ',
    style : TextStyle(
    color : Colors.white,
    fontSize: 25,
    //fontWeight: FontWeight.bold
    fontFamily: 'Proxima',
    letterSpacing: 1.3
    )),
    SizedBox(height: size.height/13),

    Center(
    child: CircleAvatar(
    backgroundImage: AssetImage('Assets/betlogo.jpg'),
    radius: 50,
    ),
    ),
          SizedBox(height: 20,),
          NestedScrollView(
            headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  pinned: true,
                  floating: true,
                  forceElevated: innerBoxIsScrolled,
                  bottom: TabBar(
                    unselectedLabelColor: Colors.black,
                    labelColor: Color(0xFF2661FA),
                    tabs: [
                      Tab(
                        icon: Icon(Icons.people),
                        text: 'Friendlybet',
                      ),

                      Tab(
                          icon: Icon(Icons.person),
                          text : 'alone bet'
                      )
                    ],
                    onTap: (index) {
                      setState(() {
                        _selectedIndex = index;
                      });
                    },
                    controller: _tabController,
                    indicatorSize: TabBarIndicatorSize.tab,
                  ),
                )
              ];
            },
            body: TabBarView(
              children: <Widget>[
                c1(), c1()
              ],
              controller: _tabController,
            )
          ),

          SingleChildScrollView(
            child: pagesAll.elementAt(_selectedIndex),
          )



          ]
    ),
    ),
    ),
    );
  }
}
