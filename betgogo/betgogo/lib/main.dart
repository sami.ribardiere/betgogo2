import 'package:betgogo/Home/createbet.dart';
import 'package:betgogo/Home/updateform.dart';
import 'package:flutter/material.dart';
import 'package:betgogo/login/background.dart';
import 'package:provider/provider.dart';

import 'Home/creatform.dart';
import 'Home/home.dart';
import 'designformdisplay.dart';
import 'login/log.dart';
import 'model/storedid.dart';
void main() {
  runApp(ChangeNotifierProvider(
      create: (BuildContext context) => ID(0),
      child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {


    return MaterialApp (

        initialRoute: '/login',
        routes: {
          '/login' : (context) => LoginScreen(),
          '/createform' : (context) => cform(),
          '/cbet' : (context) => cbet(),
          '/home' : (context) => home(i : 0,),
          '/list' : (context) => dform(),

        }
    );

  }
}

