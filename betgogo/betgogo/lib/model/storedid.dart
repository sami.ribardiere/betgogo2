import 'package:flutter/material.dart';

class ID extends ChangeNotifier{
  int id;

  ID(this.id);

  int getid() => this.id;

  void setID(int value) => this.id = value;

}