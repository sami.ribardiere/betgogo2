import 'dart:io';

import 'package:betgogo/login/background.dart';
import 'package:betgogo/login/log.dart';
import 'package:betgogo/login/verifyemail.dart';
import 'package:flutter/material.dart';

import 'package:image_picker/image_picker.dart';
import 'package:lottie/lottie.dart';
import 'dart:convert';
import '../Home/image_controller.dart';
import '../register.dart';
import 'package:http/http.dart' as http;
import 'package:get/get.dart';






class RegisterScreen extends StatefulWidget {
  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {


 File? _image;
 PickedFile? _pickedFile;
  final _picker = ImagePicker();
  Future<void> pickImage() async {
    _pickedFile = await _picker.getImage(source: ImageSource.gallery);
    if(_pickedFile != null){
      setState((){
        _image = File(_pickedFile!.path);
      });
    }
  }







  final _formKey = GlobalKey<FormState>();
  register reg = register('', '', '', '');

  // http backen connection
  var url = Uri.parse('http://10.0.2.2:8000/api/register');

  Future save() async {
    print('toto');

    http.MultipartRequest res = http.MultipartRequest('POST', url,);
    res.fields['email'] = reg.email;
    res.fields['name'] = 'aaa';
    res.fields['password'] = reg.Password;
    res.fields['username'] = reg.Username;

    if (GetPlatform.isMobile && _pickedFile != null) {
      File _file = File(_pickedFile!.path);
      res.files.add(http.MultipartFile.fromBytes(
          'profilepic', File(_pickedFile!.path).readAsBytesSync(),
          filename: _pickedFile!.path
              .split('/')
              .last));
    }

    var ress = await res.send();


    if (ress.statusCode == 200) {

      Map data = jsonDecode(await ress.stream.bytesToString()) as Map;
      print('--------------------------------');
      String msg = data['msg'];
      int code = data['code'];
      int id = data['id'];
      //String token = data['accessToken'];
      print(msg);
      AlertDialog alert = AlertDialog(
        title: Center(child: Icon(
          Icons.close,
          color : Colors.red,
          size: 50,
        )
        ),
        content: Text(
          msg,
          textAlign: TextAlign.center,
        ),
        actions: [
          TextButton(
            child: Center(child: Text("Cancel")),
            onPressed:  () {
              Navigator.of(context).pop();
            },
          )
        ],
      );


      if(msg=='success'){
        Navigator.push(context, MaterialPageRoute(builder: (context) => verifyemail(id: id)));

      }else if(msg!='success'){
        print('aaa');

        showDialog(
          context: context,
          builder: (BuildContext context) {
            return alert;
          },
        );
      }


    }
  }




  @override
  Widget build(BuildContext context) {


    Size size = MediaQuery.of(context).size;

    return Scaffold(
        body: Background(
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Text(
                        "REGISTER",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color(0xFF963096),
                            fontSize: 36
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),


                    SizedBox(height: size.height * 0.05),
                    ////////////////////////////////////////image upload///////////////////////////////////////

                   Text('Profile Picture',
                   style: TextStyle(
                     fontSize: 17,
                   ),),

                    SizedBox(height: size.height * 0.01),




                    Container(
                        child: GestureDetector(
                          child:  Container(child: _pickedFile != null
                              ? Center(
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(20), // Image border
                              child: SizedBox.fromSize(
                                size: Size.fromRadius(50), // Image radius
                                child: Image.file(File(_pickedFile!.path),
                                    fit: BoxFit.cover),
                              ),

                            ),
                          )
                              : ClipRRect(
                            borderRadius: BorderRadius.circular(20), // Image border
                            child: SizedBox.fromSize(
                              size: Size.fromRadius(50), // Image radius
                              child: Image.asset('Assets/profile2.jpg',fit: BoxFit.cover),

                            ),

                          ),
                          ),
                          onTap: () => pickImage(),
                        )
                    ),
                    SizedBox(height: size.height * 0.00),



                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.symmetric(horizontal: 40),
                      child: TextFormField(
                        decoration: InputDecoration(
                            labelText: "Username"
                        ),
                        controller: TextEditingController(text: reg.Username),
                        onChanged: (val) {
                          reg.Username = val;
                        },
                        validator: (value) {
                          if (value!.length < 2) {
                            return 'Username is not Valid';
                          }
                          return null;
                        },

                      ),
                    ),

                    SizedBox(height: size.height * 0.03),

                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.symmetric(horizontal: 40),
                      child: TextFormField(
                        decoration: InputDecoration(
                            labelText: "Email Adress"
                        ),
                        controller: TextEditingController(text: reg.email),
                        onChanged: (val) {
                          reg.email = val;
                        },
                        validator: (value) {
                          if (!value!.contains(RegExp(
                              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+"))) {
                            return 'Email is not valid';
                          }
                          return null;
                        },

                      ),
                    ),






                    ////////////////////////////////////////image upload///////////////////////////////////////

                    SizedBox(height: size.height * 0.03),

                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.symmetric(horizontal: 40),
                      child: TextFormField(
                        decoration: InputDecoration(
                            labelText: "Password"
                        ),
                        controller: TextEditingController(text: reg.Password),
                        onChanged: (val) {
                          reg.Password = val;
                        },
                        validator: (value) {
                          if (!value!.contains(
                              RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])')) ||
                              value.length < 8) {
                            return 'Passsord is not valid';
                          }
                          return null;
                        },
                        obscureText: true,

                      ),
                    ),

                    SizedBox(height: size.height * 0.05),

                    Container(
                      alignment: Alignment.centerRight,
                      margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                      child: RaisedButton(
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                            await Future.delayed(Duration(seconds: 1), () {
                              save();
                            });
                            showDialog(context: context,
                                builder: (BuildContext context) =>
                                    SimpleDialog(
                                        title: const Text(
                                          'SIGNING UP .......',
                                          style: TextStyle(
                                            //fontWeight: FontWeight.bold,
                                            fontSize: 30,
                                            fontStyle: FontStyle.italic,
                                          ),
                                        ),

                                        children: <Widget>[
                                          Lottie.asset('lotties/loading-blue.json'),
                                        ]


                                    ));
                          }
                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(80.0)),
                        textColor: Colors.white,
                        padding: const EdgeInsets.all(0),
                        child: Container(
                          alignment: Alignment.center,
                          height: 50.0,
                          width: size.width * 0.5,
                          decoration: new BoxDecoration(
                              borderRadius: BorderRadius.circular(80.0),
                              gradient: new LinearGradient(
                                  colors: [
                                    Color(0xFF963096),
                                    Color(0xFF2661FA),
                                  ]
                              )
                          ),
                          padding: const EdgeInsets.all(0),
                          child: Text(
                            "SIGN UP",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold
                            ),
                          ),
                        ),
                      ),
                    ),

                    Container(
                      alignment: Alignment.centerRight,
                      margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                      child: GestureDetector(
                        onTap: () =>
                        {
                          Navigator.push(context, MaterialPageRoute(
                              builder: (context) => LoginScreen()))
                        },
                        child: Text(
                          "Already Have an Account? Sign in",
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              color: Color(0xFF963096)
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),



        ));

  }
}
/*import 'dart:io';

import 'package:betgogo/login/background.dart';
import 'package:betgogo/login/log.dart';
import 'package:betgogo/login/verifyemail.dart';
import 'package:flutter/material.dart';

import 'package:image_picker/image_picker.dart';
import 'package:lottie/lottie.dart';
import 'dart:convert';
import '../Home/image_controller.dart';
import '../register.dart';
import 'package:http/http.dart' as http;
import 'package:get/get.dart';






class RegisterScreen extends StatefulWidget {
  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {


 File? _image;
 PickedFile? _pickedFile;
  final _picker = ImagePicker();
  Future<void> pickImage() async {
    _pickedFile = await _picker.getImage(source: ImageSource.gallery);
    if(_pickedFile != null){
      setState((){
        _image = File(_pickedFile!.path);
      });
    }
  }







  final _formKey = GlobalKey<FormState>();
  register reg = register('', '', '', '');

  // http backen connection
  var url = Uri.parse('http://10.0.2.2:8000/api/register');

  Future save() async {
    print('toto');

    var res = await http.post(url,
        headers: {'Content-Type': 'application/json; charset=UTF-8',},
        body: json.encode({

          'email': reg.email,
          'name': 'toto',
          'password': reg.Password,
          'username': reg.Username,
          'profilepic' : 'toto'
        })
    );
    print(reg.email);
    if (res.statusCode == 200) {

      Map data = jsonDecode(res.body) as Map;
      print('--------------------------------');
      String msg = data['msg'];
      int code = data['code'];
      int id = data['id'];
      //String token = data['accessToken'];
      print(msg);
      AlertDialog alert = AlertDialog(
        title: Center(child: Icon(
          Icons.close,
          color : Colors.red,
          size: 50,
        )
        ),
        content: Text(
          msg,
          textAlign: TextAlign.center,
        ),
        actions: [
          TextButton(
            child: Center(child: Text("Cancel")),
            onPressed:  () {
              Navigator.of(context).pop();
            },
          )
        ],
      );


      if(msg=='success'){
        Navigator.push(context, MaterialPageRoute(builder: (context) => verifyemail(id: id)));
        print(res.body);
      }else if(msg!='success'){
        print('aaa');
        print(res.body);
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return alert;
          },
        );
      }
      print(res.body);

    }
  }




  @override
  Widget build(BuildContext context) {


    Size size = MediaQuery.of(context).size;

    return Scaffold(
        body: Background(
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.symmetric(horizontal: 40),
                      child: Text(
                        "REGISTER",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color(0xFF963096),
                            fontSize: 36
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),

                    SizedBox(height: size.height * 0.03),

                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.symmetric(horizontal: 40),
                      child: TextFormField(
                        decoration: InputDecoration(
                            labelText: "Username"
                        ),
                        controller: TextEditingController(text: reg.Username),
                        onChanged: (val) {
                          reg.Username = val;
                        },
                        validator: (value) {
                          if (value!.length < 2) {
                            return 'Username is not Valid';
                          }
                          return null;
                        },

                      ),
                    ),

                    SizedBox(height: size.height * 0.03),

                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.symmetric(horizontal: 40),
                      child: TextFormField(
                        decoration: InputDecoration(
                            labelText: "Email Adress"
                        ),
                        controller: TextEditingController(text: reg.email),
                        onChanged: (val) {
                          reg.email = val;
                        },
                        validator: (value) {
                          if (!value!.contains(RegExp(
                              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+"))) {
                            return 'Email is not valid';
                          }
                          return null;
                        },

                      ),
                    ),

                    SizedBox(height: size.height * 0.03),
                    ////////////////////////////////////////image upload///////////////////////////////////////

                    Container(child: _pickedFile != null
                        ? Center(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20), // Image border
                        child: SizedBox.fromSize(
                          size: Size.fromRadius(60), // Image radius
                          child: Image.file(File(_pickedFile!.path),
                              fit: BoxFit.cover),
                        ),

                      ),
                    )
                        : CircleAvatar(child: Image.asset('Assets/profile2.jpg')),
                    ),


                    SizedBox(height: size.height * 0.03),

                    Container(
                        child: GestureDetector(
                          child: Text('Select an avatar'),
                          onTap: () => pickImage(),
                        )
                    ),


                    ////////////////////////////////////////image upload///////////////////////////////////////

                    SizedBox(height: size.height * 0.03),

                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.symmetric(horizontal: 40),
                      child: TextFormField(
                        decoration: InputDecoration(
                            labelText: "Password"
                        ),
                        controller: TextEditingController(text: reg.Password),
                        onChanged: (val) {
                          reg.Password = val;
                        },
                        validator: (value) {
                          if (!value!.contains(
                              RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])')) ||
                              value.length < 8) {
                            return 'Passsord is not valid';
                          }
                          return null;
                        },
                        obscureText: true,

                      ),
                    ),

                    SizedBox(height: size.height * 0.05),

                    Container(
                      alignment: Alignment.centerRight,
                      margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                      child: RaisedButton(
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                            await Future.delayed(Duration(seconds: 1), () {
                              save();
                            });
                            showDialog(context: context,
                                builder: (BuildContext context) =>
                                    SimpleDialog(
                                        title: const Text(
                                          'SIGNING UP .......',
                                          style: TextStyle(
                                            //fontWeight: FontWeight.bold,
                                            fontSize: 30,
                                            fontStyle: FontStyle.italic,
                                          ),
                                        ),

                                        children: <Widget>[
                                          Lottie.asset('lotties/loading-blue.json'),
                                        ]


                                    ));
                          }
                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(80.0)),
                        textColor: Colors.white,
                        padding: const EdgeInsets.all(0),
                        child: Container(
                          alignment: Alignment.center,
                          height: 50.0,
                          width: size.width * 0.5,
                          decoration: new BoxDecoration(
                              borderRadius: BorderRadius.circular(80.0),
                              gradient: new LinearGradient(
                                  colors: [
                                    Color(0xFF963096),
                                    Color(0xFF2661FA),
                                  ]
                              )
                          ),
                          padding: const EdgeInsets.all(0),
                          child: Text(
                            "SIGN UP",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold
                            ),
                          ),
                        ),
                      ),
                    ),

                    Container(
                      alignment: Alignment.centerRight,
                      margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                      child: GestureDetector(
                        onTap: () =>
                        {
                          Navigator.push(context, MaterialPageRoute(
                              builder: (context) => LoginScreen()))
                        },
                        child: Text(
                          "Already Have an Account? Sign in",
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              color: Color(0xFF963096)
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),



        ));

  }
}
*/