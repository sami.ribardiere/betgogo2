import 'package:betgogo/Home/home.dart';
import 'package:betgogo/login/background.dart';
import 'package:betgogo/login/reg.dart';
import 'package:betgogo/login/userlog.dart';
import 'package:betgogo/model/storedid.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

import '../Home/fp1.dart';





class UserModel{
  String token;
  String uuid;
  UserModel(this.token, this.uuid);
}

class LoginScreen extends StatefulWidget {

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  final _formKey = GlobalKey<FormState>();
  User user = User("","","");

  String uuid="";
  String token="";
  String msg= "";

  Future save() async {
    try {
      var url = Uri.parse('http://10.0.2.2:8000/api/login');
      var res = await http.post(url,
          headers: {'Content-Type': 'application/json; charset=UTF-8',},
          body: json.encode({
            'email': user.email,//user.username,//'Test2',//user.username,
            'password': user.password,//user.password,//'123Abc!2',//user.password,
          })
      );




      //print(user.username);
      //print(user.password);
      //print(res.body);
      if(res.statusCode==200) {
        //print(res.body);
        Map info = jsonDecode(res.body) as Map;

        String token = info['jwt'];
        int uuid = info['detail'];
        String msg = info['msg'];

        print(uuid);
        print(token);
        AlertDialog alert = AlertDialog(

          content: Text(
            msg,
            style: TextStyle(

              fontSize: 25
            ),
            textAlign: TextAlign.center,
          ),

          actions: [
            Lottie.asset('lotties/fail.json'),

            TextButton(
              child: Center(child: Text("Cancel")),
              onPressed:  () {
                Navigator.of(context).pop();
              },
            ),

          ],


        );
        ///////////////////////////////////////////////////////////// change this part later ///////////////////////////////////////////////
        if (msg == 'User not found !' || msg == 'Incorrect password !') {
          print("totvdedvdvo");
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return alert;
            },
          );
        } else {
          Provider.of<ID>(context,listen: false).setID(uuid);


          print('ref');

          Navigator.push(context, MaterialPageRoute(builder: (context) => home(i : 0,)));

        }

      }
    }catch(e){
      print('caught error2:  $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: Background(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.symmetric(horizontal: 40),
              child: Text(
                "LOGIN",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF963096),
                    fontSize: 36
                ),
                textAlign: TextAlign.left,
              ),
            ),

            SizedBox(height: size.height * 0.03),

            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 40),
              child: TextFormField(
                decoration: InputDecoration(
                    labelText: "Email"
                ),
                controller: TextEditingController(text: user.email),
                onChanged: (val){
                  user.email = val;
                },
                validator: (value){
                  if (value == ""){
                    return 'email is Empty';
                  }
                  return null;
                },
              ),
            ),


            SizedBox(height: size.height * 0.03),

            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 40),
              child: TextFormField(
                decoration: InputDecoration(
                    labelText: "Password"
                ),
                controller: TextEditingController(text: user.password),
                onChanged: (val){
                  user.password = val;
                },
                validator: (value){
                  if (value == ""){
                    return 'password is Empty';
                  }
                  return null;
                },
                style: TextStyle(
                  color: Colors.black,


                ),
                obscureText: true,
              ),
            ),


            Container(
              alignment: Alignment.centerRight,
              margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
              child: GestureDetector(
                onTap: () => {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => fp1()))
                },
                child: Text(
                  "Forgot your password ?",
                  style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF963096)
                  ),
                ),
              ),
            ),

            SizedBox(height: size.height * 0.05),

            Container(
              alignment: Alignment.centerRight,
              margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
              child: RaisedButton(
                onPressed: () {
                  save();
                  print("toto");
                },
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
                textColor: Colors.white,
                padding: const EdgeInsets.all(0),
                child: Container(
                  alignment: Alignment.center,
                  height: 50.0,
                  width: size.width * 0.5,
                  decoration: new BoxDecoration(
                      borderRadius: BorderRadius.circular(80.0),
                      gradient: new LinearGradient(
                          colors: [
                            Color(0xFF963096),
                            Color(0xFF2661FA),
                          ]
                      )
                  ),
                  padding: const EdgeInsets.all(0),
                  child: Text(
                    "LOGIN",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerRight,
              margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
              child: GestureDetector(
                onTap: () => {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => RegisterScreen()))
                },
                child: Text(
                  "Don't Have an Account? Sign up",
                  style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF963096)
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
