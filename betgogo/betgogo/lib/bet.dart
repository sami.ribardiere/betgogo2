import 'dart:typed_data';

import 'package:flutter/material.dart';

class bet {
  String name1;
  String name2;
  String description;
  int price;
  DateTime date;
  CircleAvatar p1;
  CircleAvatar p2;


  bet(this.name1, this.name2,this.description,this.price,this.date,this.p1 ,this.p2);

}

class Bet {
  int id;
  String name1;
  String name2;
  String description;
  int price;
  int user;
  String date;
  String imageurl;
  Uint8List image_memory;

  Bet(this.name1, this.name2,this.description,this.price,this.id,this.user,this.date, this.imageurl,this.image_memory);

}

class FBet {
  int id;
  String name1;
  String name2;
  String description;
  int price;
  int bet_request_id;
  String date;
  String p1imageurl;
  Uint8List p1image_memory;
  String p2imageurl;
  Uint8List p2image_memory;

  FBet(this.name1, this.name2,this.description,this.price,this.id,this.bet_request_id,this.date, this.p1imageurl,this.p1image_memory,this.p2image_memory,this.p2imageurl);

}

class FBetDisplay {
  int id;
  String name1;
  String name2;
  String description;
  int price;
  int bet_request_id;
  String date;
  String p1imageurl;
  Uint8List p1image_memory;
  String p2imageurl;
  Uint8List p2image_memory;
  String state;
  int fbet_winner_request_id;
  int bet_winner_sender_id;
  int p1_id;
  int p2_id;

  FBetDisplay(this.name1, this.name2,this.description,this.price,this.id,this.bet_request_id,this.date, this.p1imageurl,this.p1image_memory,this.p2image_memory,this.p2imageurl,this.state,this.fbet_winner_request_id,this.bet_winner_sender_id,this.p1_id,this.p2_id);

}