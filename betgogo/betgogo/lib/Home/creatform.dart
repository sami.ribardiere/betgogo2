import 'dart:convert';
import 'package:betgogo/Home/actualbackcreatebet.dart';
import 'package:http/http.dart' as http;
import 'package:betgogo/bet.dart';
import 'package:flutter/material.dart';
class cform extends StatefulWidget {
  const cform({Key? key}) : super(key: key);

  @override
  State<cform> createState() => _cformState();
}

class _cformState extends State<cform> {


  TextEditingController price = TextEditingController();
  CircleAvatar c = CircleAvatar(
    backgroundImage: AssetImage('Assets/profile.jpg'),
  );

  bet bett = bet(
      "",
      "",
      "",
      0,
      DateTime.now(),
      CircleAvatar(
        backgroundImage: AssetImage('Assets/profile.jpg'),
      ),
      CircleAvatar(
        backgroundImage: AssetImage('Assets/profile.jpg'),
      ));


  // connecting to backend

  var url = Uri.parse('http://10.0.2.2:8000/api/bet');


  Future save() async {
    print('toto');
    var res = await http.post(url,
        headers: {'Content-Type': 'application/json; charset=UTF-8',},
        body: json.encode({

          'name1': bett.name1,
          'name2': bett.name2,
          'price': bett.price,
          'description': bett.description,
          'user' : 2,
          'date' : bett.date.toString()
        })
    );
  }





  @override

  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    DateTime date = DateTime.now();
    return Container(
      height: size.height*2,
      child: Scaffold(

        body: Container(
          child: SingleChildScrollView(
            child: Column(

              children: [




                Center(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(0,20,0,30),
                    child: Container(
                      alignment: Alignment.center,
                      //width: 250,


                    ),
                  ),
                ),

                Container(
                  alignment: Alignment.topLeft,
                  width: size.width - 80,
                  child: TextFormField(
                    decoration: InputDecoration(
                        labelText: 'Partcipant 1',
                        labelStyle: TextStyle(
                          color:  Color(0xFF963096),
                        ),
                        fillColor:  Color(0xFF963096),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide(
                            color:  Color(0xFF963096),
                          ),

                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(
                            color:  Colors.grey,
                          )
                          ,
                        )
                    ),
                    controller: TextEditingController(text: bett.name1),
                    onChanged: (val){
                      bett.name1 = val;
                    },

                  ),
                ),

                SizedBox(height: 30,),

                Container(
                  alignment: Alignment.topLeft,
                  width: size.width - 80,
                  child: TextFormField(
                    decoration: InputDecoration(
                        labelText: 'Partcipant 2',
                        labelStyle: TextStyle(
                          color:  Color(0xFF963096),
                        ),
                        fillColor:  Color(0xFF963096),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide(
                            color:  Color(0xFF963096),
                          ),

                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(
                            color:  Colors.grey,
                          )
                          ,
                        )
                    ),
                    controller: TextEditingController(text: bett.name2),
                    onChanged: (val){
                      bett.name2 = val;
                    },

                  ),
                ),

                SizedBox(height: 30,),

                Container(
                  height: size.height/5,
                  alignment: Alignment.topLeft,
                  width: size.width - 80,
                  child: TextFormField(
                    keyboardType: TextInputType.multiline,
                    maxLines: 5,
                    decoration: InputDecoration(
                        labelText: 'Description',


                        labelStyle: TextStyle(

                          color:  Color(0xFF963096),
                        ),
                        fillColor:  Color(0xFF963096),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide(
                            color:  Color(0xFF963096),
                          ),

                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(
                            color:  Colors.grey,
                          )
                          ,
                        )
                    ),

                    controller: TextEditingController(text: bett.description),
                    onChanged: (val){
                      bett.description = val;
                    },

                  ),
                ),
                SizedBox(height: 20,),

                Container(
                  alignment: Alignment.topLeft,
                  width: size.width - 80,
                  child: TextFormField(
                    decoration: InputDecoration(
                        labelText: 'Amount',
                        labelStyle: TextStyle(
                          color:  Color(0xFF963096),
                        ),
                        fillColor:  Color(0xFF963096),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide(
                            color:  Color(0xFF963096),
                          ),

                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(
                            color:  Colors.grey,
                          )
                          ,
                        )
                    ),
                    keyboardType: TextInputType.number,
                    //controller: TextEditingController(text: bett.price.toString()),
                    controller: price,
                    onChanged: (val){
                      bett.price = int.parse(price.text);
                    },

                  ),
                ),

                SizedBox(height: 30,),


                Container(
                  height: 50,
                  width: 150,
                  child: ElevatedButton(onPressed: () {
                    _selectDate(context);
                  },
                    style: ElevatedButton.styleFrom(
                      primary:  Color(0xFF963096),
                    ),
                    child: Text('Select Date'),

                  ),
                ),





              ],
            ),
          ),
        ),
        floatingActionButton: Container(
          width: size.width - 40,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [

              FloatingActionButton(

                onPressed: (){
                  Navigator.pushNamed(context, '/home'); },
                child: Icon(Icons.arrow_back),
                backgroundColor:Color(0xFF2661FA) ,



              ),
              FloatingActionButton(

                onPressed: (){
                  print(bett.date);
                  save();
                  Navigator.pushNamed(context, '/home');
                  },
                child: Icon(Icons.check),
                backgroundColor:Color(0xFF2661FA) ,



              ),
            ],
          ),
        ),
      ),
    );
  }
  _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(), // Refer step 1
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != bett.date)
      setState(() {
        bett.date = picked;
      });
  }

}
