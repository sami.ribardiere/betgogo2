import 'package:flutter/material.dart';
import 'dart:ui' as ui;

import 'backgroundprofile.dart';

class actualback extends StatelessWidget {
  final Widget child;

  const actualback({Key? key, required this.child,}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      child: Stack(

        children: [
          Positioned(
            top: 0,
            left: 0,

            child: Stack(
              clipBehavior: Clip.none,

              children: [
                CustomPaint(
                  size: Size(size.width, size.height),
                  //You can Replace [WIDTH] with your desired width for Custom Paint and height will be calculated automatically
                  painter: RPSCustomPainter(),
                ),
                child
              ],
            ),
          ),



        ],


      ),

    );

  }
}
/*
 CustomPaint(
          size: Size(size.width,size.height) ,//You can Replace [WIDTH] with your desired width for Custom Paint and height will be calculated automatically
          painter: RPSCustomPainter(),
        ),
 */