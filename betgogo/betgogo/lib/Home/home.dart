
import 'package:betgogo/Home/Bet_pending.dart';
import 'package:betgogo/Home/Profile2.dart';
import 'package:betgogo/Home/acutal%20back.dart';
import 'package:betgogo/Home/createbet.dart';
import 'package:betgogo/Home/profilepage.dart';
import 'package:betgogo/appbar/creates_bets.dart';
import 'package:flutter/material.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:betgogo/globals.dart' as globals;
import '../designformdisplay.dart';
import 'Create_new_bet.dart';
import 'Display_friendly_bet.dart';
import 'creatform.dart';
import 'form.dart';
class home extends StatefulWidget {
  int i;

home({Key? key, required this.i}) : super(key: key);

  @override
  State<home> createState() => _homeState();
}

class _homeState extends State<home> {

  //GlobalKey<GNavState> _NavKey = GlobalKey();
  int b = globals.id;
  var pagesAll = [create_new_bet(),Display_friendly_bet(),profile2(),Bet_Pending()];

  var myindex = 0;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(

      bottomNavigationBar: GNav(

        //key: _NavKey,
        backgroundColor: Colors.white,
        activeColor: Color(0xFF963096),
        tabBackgroundColor: Color(0xFFC034C0).withOpacity(0.2),


        gap: 1,
       tabs:[
         GButton(icon: Icons.add,
         text: 'Create Bet',),
         GButton(icon: Icons.euro,
           text: 'Your Bets',),
         GButton(icon: Icons.person  ,
           text: 'Profile',),
         GButton(icon: Icons.money  ,
           text: 'Create_new_bet',),


       ],
        selectedIndex: widget.i,
        onTabChange: (index) {
          setState(() {
            widget.i = index;
          });
        },


    ),
      body: Center (child: pagesAll.elementAt(widget.i),)
    );
  }
}
