import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../bet.dart';
import 'actualbackcreatebet.dart';
import 'home.dart';

class updateform extends StatefulWidget {

  Bet beti;

  updateform({Key? key, required this.beti}) : super(key: key);

  @override
  State<updateform> createState() => _updateformState();
}

class _updateformState extends State<updateform> {
  TextEditingController price = TextEditingController();



  // connecting to backend

  var url = Uri.parse('http://10.0.2.2:8000/api/bet');


  Future update() async {
    print('toto');
    var res = await http.put(url,
        headers: {'Content-Type': 'application/json; charset=UTF-8',},
        body: json.encode({

          'id' : widget.beti.id,
          'name1': widget.beti.name1,
          'name2': widget.beti.name2,
          'price': widget.beti.price,
          'description': widget.beti.description,
          'user' : 2,
          'date' : widget.beti.date.toString()
        })
    );
  }
  Future delete() async {

    var res = await http.delete(url,
        headers: {'Content-Type': 'application/json; charset=UTF-8',},
        body: json.encode({
          'id' : widget.beti.id
        })
    );

  }
  @override
  Widget build(BuildContext context) {
    int index = 1;
    Size size = MediaQuery.of(context).size;
    DateTime date = DateTime.now();
    return Scaffold(
      body: SingleChildScrollView(
        child: actualbackcbet(
          child: SingleChildScrollView(
            child: Container(
              width: size.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: size.height/10),
                  Text('Edit your bet here ! ',
                      style : TextStyle(
                          color : Colors.white,
                          fontSize: 25,
                          //fontWeight: FontWeight.bold
                          fontFamily: 'Proxima',
                          letterSpacing: 1.3
                      )),
                  SizedBox(height: size.height/11),

                  Center(
                    child: CircleAvatar(
                      backgroundColor: Color(0xFF963096),
                      radius: 30,
                      child: IconButton(onPressed: () {
                        delete();
                      Navigator.push(context, MaterialPageRoute(builder: (context) => home(i : 1)));
                      }, icon: Icon(Icons.delete),
                        
                      ),
                    ),
                  ),
                  SizedBox(height: 20,),
                  Container(
                    width: size.width-60,
                    child: TextFormField(
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color : Colors.transparent),
                          borderRadius: BorderRadius.circular(5.5),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color : Colors.transparent),
                          borderRadius: BorderRadius.circular(5.5),
                        ),
                        prefixIcon: Icon(
                          Icons.person,
                          color: Color(0xFF2661FA),
                        ),
                        hintText : "Enter Participant 1",
                        hintStyle :  TextStyle(color :  Colors.black),
                        filled :  true,
                        fillColor : Color(0xFF2661FA).withOpacity(0.2),
                      ),
                      controller: TextEditingController(text: widget.beti.name1),
                      onChanged: (val){
                        widget.beti.name1 = val;
                      },
                    ),
                  ),

                  SizedBox(height: 20,),

                  Container(
                    width: size.width-60,
                    child: TextFormField(
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color : Colors.transparent),
                          borderRadius: BorderRadius.circular(5.5),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color : Colors.transparent),
                          borderRadius: BorderRadius.circular(5.5),
                        ),
                        prefixIcon: Icon(
                          Icons.person,
                          color: Color(0xFF2661FA),
                        ),
                        hintText : "Enter Participant 2",
                        hintStyle :  TextStyle(color :  Colors.black),
                        filled :  true,
                        fillColor : Color(0xFF2661FA).withOpacity(0.2),
                      ),
                      controller: TextEditingController(text: widget.beti.name2),
                      onChanged: (val){
                        widget.beti.name2 = val;
                      },
                    ),
                  ),
                  SizedBox(height: 20,),

                  Container(
                    alignment: Alignment.topLeft,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(30, 0, 0, 0),
                      child: Text('Bet Description :',
                        style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.w400,

                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 8,),

                  Container(
                    width: size.width-60,
                    child: TextFormField(
                      keyboardType: TextInputType.multiline,
                      maxLines: 5,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color : Colors.transparent),
                          borderRadius: BorderRadius.circular(5.5),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color : Colors.transparent),
                          borderRadius: BorderRadius.circular(5.5),
                        ),
                        /*prefixIcon: Icon(
                          Icons.person,
                          color: Color(0xFF2661FA),
                        ), */
                        // hintText : "Description",

                        hintStyle :  TextStyle(color :  Colors.black),
                        filled :  true,
                        fillColor : Color(0xFF2661FA).withOpacity(0.2),
                      ),
                      controller: TextEditingController(text: widget.beti.description),
                      onChanged: (val){
                        widget.beti.description = val;
                      },
                    ),
                  ),
                  SizedBox(height: 20,),

                  Container(
                    width: size.width-60,
                    child: TextFormField(
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color : Colors.transparent),
                          borderRadius: BorderRadius.circular(5.5),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color : Colors.transparent),
                          borderRadius: BorderRadius.circular(5.5),
                        ),
                        prefixIcon: Icon(
                          Icons.money_sharp,
                          color: Color(0xFF2661FA),
                        ),
                        hintText : "Amount ",
                        hintStyle :  TextStyle(color :  Colors.black),
                        filled :  true,
                        fillColor : Color(0xFF2661FA).withOpacity(0.2),
                      ),
                      keyboardType: TextInputType.number,
                      //controller: TextEditingController(text: bett.price.toString()),
                      controller: price,
                      onChanged: (val){
                        widget.beti.price = int.parse(price.text);
                      },
                    ),
                  ),
                  SizedBox(height: 20,),

                  Container(
                    height: 50,
                    width: 150,
                    child: ElevatedButton(onPressed: () {
                      _selectDate(context);
                    },
                      style: ElevatedButton.styleFrom(
                        primary:  Color(0xFF2661FA),
                      ),
                      child: Text('Select Date'),

                    ),
                  ),

                  SizedBox(height: 10,),
/*
                  Container(
                   // alignment: Alignment.centerRight,
                    margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                    child: RaisedButton(
                      onPressed: () async {





                      },
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
                      textColor: Colors.white,
                      padding: const EdgeInsets.all(0),
                      child: Container(
                        alignment: Alignment.center,
                        height: 50.0,
                        width: size.width * 0.5,
                        decoration: new BoxDecoration(
                            borderRadius: BorderRadius.circular(80.0),
                            gradient: new LinearGradient(
                                colors: [
                                  Color(0xFF963096),
                                  Color(0xFF2661FA),
                                ]
                            )
                        ),
                        padding: const EdgeInsets.all(0),
                        child: Text(
                          "Submit",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                    ),
                  ),
                  */

                ],
              ),
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(

        onPressed: (){
          update();

          Navigator.push(context, MaterialPageRoute(builder: (context) => home(i : 1)));
        },
        child: Icon(Icons.edit),
        backgroundColor:Color(0xFF2661FA) ,



      ),

    );
  }
  _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(), // Refer step 1
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != widget.beti.date)
      setState(() {
        widget.beti.date = picked as String;
      });
  }
}

