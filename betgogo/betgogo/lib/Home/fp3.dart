import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:lottie/lottie.dart';

import 'dart:convert';

import '../login/background.dart';
import '../login/log.dart';
import '../login/userlog.dart';
import 'fp2.dart';


class fp3 extends StatefulWidget {

  int id;
  fp3({Key? key,required this.id }) : super(key: key);

  @override
  State<fp3> createState() => _fp3State();
}

class _fp3State extends State<fp3> {
  User user = User("","","");
  int usercode = 0;
  TextEditingController codee = TextEditingController();

  var url = Uri.parse('http://10.0.2.2:8000/api/ChangePassword');

  Future save() async{

    var res = await http.put(url,
        headers: {'Content-Type': 'application/json; charset=UTF-8',},
        body: json.encode({

          'userid' : widget.id,
          'newpassword' : user.password


        })
    );
    if (res.statusCode == 200) {
      Map data = jsonDecode(res.body) as Map;
      String msg = data['msg'];


      AlertDialog alert = AlertDialog(

        content: Text(
          msg,
          textAlign: TextAlign.center,
        ),

        actions: [
          Lottie.asset('lotties/fail.json'),

          TextButton(
            child: Center(child: Text("Cancel")),
            onPressed:  () {
              Navigator.of(context).pop();
            },
          ),






        ],


      );



      AlertDialog alert2 = AlertDialog(

        content: Text(
          msg,
          textAlign: TextAlign.center,
        ),
        actions: [
          Lottie.asset('lotties/96237-success.json'),
          TextButton(
            child: Center(child: Text("Continue")),
            onPressed:  () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => LoginScreen()));
            },
          )
        ],
      );

      if (msg == "Password Successfully changed"){

        showDialog(
          context: context,
          builder: (BuildContext context) {
            return alert2;
          },
        );
      } else{
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return alert;
          },
        );

      }
    }
  }



  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Background(
        child: Column(
          children: [
            SizedBox(height: size.height/2.6 ),
            Text('Please enter new password here',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 20,

              ),),
            SizedBox(height: size.height/15 ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 40),
              child: TextFormField(
                decoration: InputDecoration(
                    labelText: "password"
                ),
                controller: TextEditingController(text: user.password),
                onChanged: (val){
                  user.password = val;
                },
                validator: (value){
                  if (value == ""){
                    return 'password is Empty';
                  }
                  return null;
                },
              ),
            ),
          ],
        )
        ,),
      floatingActionButton : FloatingActionButton(

        onPressed: (){

          save();

        },
        child: Icon(Icons.check),
        backgroundColor:Color(0xFF963096) ,



      ),

    );
  }
}
