import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:lottie/lottie.dart';

import 'dart:convert';

import '../login/background.dart';
import '../login/log.dart';
import '../login/userlog.dart';
import 'fp2.dart';


class fp1 extends StatefulWidget {


  fp1({Key? key}) : super(key: key);

  @override
  State<fp1> createState() => _fp1State();
}

class _fp1State extends State<fp1> {
  User user = User("","","");
  int usercode = 0;
  TextEditingController codee = TextEditingController();

  var url = Uri.parse('http://10.0.2.2:8000/api/ChangePassword');

  Future save() async{

    var res = await http.post(url,
        headers: {'Content-Type': 'application/json; charset=UTF-8',},
        body: json.encode({

          'email' : user.email


        })
    );
    if (res.statusCode == 200) {
      Map data = jsonDecode(res.body) as Map;
      String msg = data['msg'];
      int userid = data['userid'];

      AlertDialog alert = AlertDialog(

        content: Text(
          msg,
          textAlign: TextAlign.center,
        ),

        actions: [
          Lottie.asset('lotties/fail.json'),

          TextButton(
            child: Center(child: Text("Cancel")),
            onPressed:  () {
              Navigator.of(context).pop();
            },
          ),






        ],


      );



      AlertDialog alert2 = AlertDialog(

        content: Text(
          msg,
          textAlign: TextAlign.center,
        ),
        actions: [
          Lottie.asset('lotties/96237-success.json'),
          TextButton(
            child: Center(child: Text("Continue")),
            onPressed:  () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => fp2(id: userid,)));
            },
          )
        ],
      );

      if (msg == "Valid Email"){

        showDialog(
          context: context,
          builder: (BuildContext context) {
            return alert2;
          },
        );
      } else{
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return alert;
          },
        );

      }
    }
  }



  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Background(
        child: Column(
          children: [
            SizedBox(height: size.height/2.6 ),
            Text('Please enter your email adress here',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 20,

              ),),
            SizedBox(height: size.height/15 ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 40),
              child: TextFormField(
                decoration: InputDecoration(
                    labelText: "Email"
                ),
                controller: TextEditingController(text: user.email),
                onChanged: (val){
                  user.email = val;
                },
                validator: (value){
                  if (value == ""){
                    return 'email is Empty';
                  }
                  return null;
                },
              ),
            ),
          ],
        )
        ,),
      floatingActionButton : FloatingActionButton(

        onPressed: (){

          save();

        },
        child: Icon(Icons.check),
        backgroundColor:Color(0xFF963096) ,



      ),

    );
  }
}
