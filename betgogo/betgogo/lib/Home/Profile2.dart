

import 'package:betgogo/Home/acutal%20back.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'dart:convert';
import '../model/storedid.dart';
import 'ProfileUser.dart';

class profile2 extends StatefulWidget {
  const profile2({Key? key}) : super(key: key);

  @override
  State<profile2> createState() => _profile2State();
}
ProfileUser user = ProfileUser("", "", "", 0, 0, base64Decode('toto'), "");
var url = Uri.parse('http://10.0.2.2:8000/api/Profile');
int idd = 0;
Future save(int id) async {
  idd = id;

  var res = await http.post(url,
      headers: {'Content-Type': 'application/json; charset=UTF-8',},
      body: json.encode({
        'userid' : id,
      })
  );
  if (res.statusCode == 200) {
    Map data = jsonDecode(res.body) as Map;

    //user = ProfileUser(data['name'], data['email'], data['username'], data['betplayed'], data['betwon'], base64Decode(data["image_memory"]), data['profilepic']);
    user.Username = data['username'];
    user.image_memory =  base64Decode(data["image_memory"]);
    user.betplayed =  data['betplayed'];
    user.betwon = data['betwon'];
    print(user.Username);
  }

}


class _profile2State extends State<profile2> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      child : FutureBuilder(
        future : save(Provider.of<ID>(context).getid()),
        builder: (context, snapshot) {

            return actualback(
                child: Container(
                  width: size.width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: size.height/14,),
                      Center(
                        child: Text('Profile',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 21,
                          ),
                        ),
                      ),
                      SizedBox(height:size.height/100),
                      Center(
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(20), // Image border
                          child: SizedBox.fromSize(
                            size: Size.fromRadius(60), // Image radius
                            child: Image(image:MemoryImage(user.image_memory),fit: BoxFit.cover)

                          ),
                        ),
                      ),
                      SizedBox(height:size.height/50),
                      Center(
                        child: Text(user.Username,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                          ),
                        ),
                      ),
                      SizedBox(height:size.height/50),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Column(
                            children: [
                              Text('Total bets ',
                                style: TextStyle(
                                  fontWeight: FontWeight.w300,
                                  color: Colors.grey,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
                                child: Text(user.betplayed.toString(),
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.red[400],
                                      fontWeight: FontWeight.w300
                                  ),),
                              )
                            ],
                          ),
                          Column(
                            children: [
                              Text('Winning bet',
                                style: TextStyle(
                                  fontWeight: FontWeight.w300,
                                  color: Colors.grey,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
                                child: Text(user.betwon.toString(),
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.red[400],
                                      fontWeight: FontWeight.w300
                                  ),),
                              )
                            ],
                          ),
                        ],
                      ),
                      SizedBox(height:size.height/40 ),

                      FloatingActionButton(onPressed: (){},
                        child: Icon(Icons.edit),
                        backgroundColor: Color(0xFF2661FA),
                      ),




                    ],
                  ),
                )
            );

        },


      ),
    );
  }
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => save(idd));
  }
}
 /*
 Container(
      child: actualback(
          child: Container(
            width: size.width,
            child: Column(
             crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: size.height/14,),
                Center(
                  child: Text('Profile',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 21,
                    ),
                  ),
                ),
                SizedBox(height:15),
                Center(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20), // Image border
                    child: SizedBox.fromSize(
                      size: Size.fromRadius(60), // Image radius
                      child: Image.asset('Assets/profile2.jpg', fit: BoxFit.cover),
                    ),
                  ),
                ),
                SizedBox(height:15),
                Center(
                  child: Text(user.Username,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                ),
                SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                  Column(
                    children: [
                      Text('Total bets ',
                      style: TextStyle(
                        fontWeight: FontWeight.w300,
                        color: Colors.grey,
                      ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
                        child: Text('8',
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.red[400],
                          fontWeight: FontWeight.w300
                        ),),
                      )
                    ],
                  ),
                    Column(
                      children: [
                        Text('Winning bet',
                          style: TextStyle(
                            fontWeight: FontWeight.w300,
                            color: Colors.grey,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
                          child: Text('3',
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.red[400],
                                fontWeight: FontWeight.w300
                            ),),
                        )
                      ],
                    ),
                ],
                ),
                SizedBox(height: 12 ),

                FloatingActionButton(onPressed: (){},
                  child: Icon(Icons.edit),
                  backgroundColor: Color(0xFF2661FA),
                ),




              ],
            ),
          )
      ),
    );
  */