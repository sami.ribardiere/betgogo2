import 'package:betgogo/Home/Create_new_bet.dart';
import 'package:betgogo/widget/Friendlybet.dart';
import 'package:betgogo/widget/pendingbetrequest.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'dart:convert';

import '../bet.dart';
import '../model/storedid.dart';
import 'home.dart';
class Display_friendly_bet extends StatefulWidget {
  const Display_friendly_bet({Key? key}) : super(key: key);

  @override
  State<Display_friendly_bet> createState() => _Display_friendly_betState();
}

class _Display_friendly_betState extends State<Display_friendly_bet> {

  int n = 0;
  List<FBetDisplay> display = [];
  int id = 0;

  var url = Uri.parse('http://10.0.2.2:8000/api/friendlybetDisplay');
  Future save(int id) async {
    var res = await http.post(url,
        headers: {'Content-Type': 'application/json; charset=UTF-8',},
        body: json.encode({
          'userid': id,
        })
    );
    print('avant code 200');
    if (res.statusCode == 200) {
      print('test');

      Map data = jsonDecode(res.body) as Map;
      print('test2');
      var a = data['msg'];
      var b = data['bet_request_id'];

      // don't forget in the futur to add bet_request_id

      for(var u in a){
        FBetDisplay bet = FBetDisplay("", "", "", 0, 0, 0, "", "", base64Decode("image_memory"),base64Decode("image_memory"),"","",0,0,0,0);
        print('toto1');
        bet.description = u['description'];
        bet.date = u['date'];
        bet.price = u['price'];
        bet.id = u['id'];
        print('toto2');
        Map state = u['state'];
        String _state = state['state'];
        bet.state = _state;
        print('toto3');
        if (_state == 'Pending'){
          Map info = state['info'];
          bet.bet_winner_sender_id = info['bet_winner_sender'];
          bet.fbet_winner_request_id = info['winner_request'];
        }
        print('toto4');

        var people = u['people'];
        for(var p in people){
          if(bet.name1 == ''){
            bet.name1 = p['username'];
            bet.p1imageurl = p['profilepic'];
            bet.p1image_memory = base64Decode(p['image_memory']);
            bet.p1_id = p['id'];
          }
          else{
            bet.name2 = p['username'];
            bet.p2imageurl = p['profilepic'];
            bet.p2image_memory = base64Decode(p['image_memory']);
            bet.p2_id = p['id'];
          }
        }
        display.add(bet);
        print(display[0]);
        print(display.length);
        print(bet.name1);








        /*Bet bett = Bet(email,email,email,u['price'],1,1,email,email,base64Decode(email));
        display.add(bett);*/

      }



    }
    return display;
  }
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: FutureBuilder(
        future : save(Provider.of<ID>(context).getid()),
        builder: (context, snapshot) {
          if (snapshot.data == null) {
            return Center(child: Container(
                width: 100,
                height: 100,
                child: Lottie.asset('lotties/loading-blue.json')));
          } else
              if (display.length == 0){
                return Container(
                  alignment: Alignment.centerRight,
                  margin: EdgeInsets.symmetric(horizontal: size.width/4, vertical: 0),
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => home(i:0)));
                    },
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
                    textColor: Colors.white,
                    padding: const EdgeInsets.all(0),
                    child: Container(
                      alignment: Alignment.center,
                      height: 50.0,
                      width: size.width * 0.5,
                      decoration: new BoxDecoration(
                          borderRadius: BorderRadius.circular(80.0),
                          gradient: new LinearGradient(
                              colors: [
                                Color(0xFF963096),
                                Color(0xFF2661FA),
                              ]
                          )
                      ),
                      padding: const EdgeInsets.all(0),
                      child: Text(
                        "Create your first bet !",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                  ),
                );
              } else { return ListView.builder(
                  itemCount: display.length,
                  itemBuilder: (BuildContext context,int index) {
                    return friendlybet(mybet: display[index]);
                  }); }

        },


      ),


    );

  }
  @override
  void initState() {
    display.clear();



  }
}
