import 'package:flutter/cupertino.dart';

class RPSCustomPainter2 extends CustomPainter{

  @override
  void paint(Canvas canvas, Size size) {



    Paint paint0 = Paint()
      ..color = const Color(0xFF2661FA)
      ..style = PaintingStyle.fill
      ..strokeWidth = 1;


    Path path0 = Path();
    path0.moveTo(0,size.height*0.2357287);
    path0.quadraticBezierTo(size.width*0.5770286,size.height*0.2597448,size.width,size.height*0.2038348);
    path0.cubicTo(size.width*1.0109571,size.height*0.2125655,size.width*1.0071000,size.height*0.2009402,size.width,size.height*0.2096709);
    path0.quadraticBezierTo(size.width*0.8565429,size.height*0.2429550,size.width*0.0010143,size.height*0.2750907);
    path0.lineTo(0,size.height*0.2357287);
    path0.close();

    canvas.drawPath(path0, paint0);


  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }

}
class RPSCustomPainter3 extends CustomPainter{

  @override
  void paint(Canvas canvas, Size size) {



    Paint paint0 = Paint()
      ..color = const Color(0xFF2661FA)
      ..style = PaintingStyle.fill
      ..strokeWidth = 1;


    Path path0 = Path();
    path0.moveTo(0,size.height*0.1846877);
    path0.cubicTo(size.width*-0.0014286,size.height*0.2244795,0,size.height*0.2043318,0,size.height*0.2209537);
    path0.cubicTo(size.width*0.1642857,size.height*0.1939221,size.width*0.8285714,size.height*0.2446273,size.width,size.height*0.2625923);
    path0.cubicTo(size.width*1.0042857,size.height*0.2548690,size.width*1.0014286,size.height*0.2642713,size.width,size.height*0.2565480);
    path0.cubicTo(size.width*0.8214286,size.height*0.2343855,size.width*0.2328571,size.height*0.1638684,0,size.height*0.1846877);
    path0.close();

    canvas.drawPath(path0, paint0);


  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }

}
class RPSCustomPainter4 extends CustomPainter{

  @override
  void paint(Canvas canvas, Size size) {



    Paint paint0 = Paint()
      ..color = const Color(0xFF2661FA)
      ..style = PaintingStyle.fill
      ..strokeWidth = 1;


    Path path0 = Path();
    path0.moveTo(0,size.height*0.0006716);
    path0.lineTo(size.width,0);
    path0.lineTo(size.width,size.height*0.2691404);
    path0.quadraticBezierTo(size.width*0.7589286,size.height*0.2291807,size.width*0.7142857,size.height*0.2350571);
    path0.cubicTo(size.width*0.5357143,size.height*0.2434520,size.width*0.2267857,size.height*0.3265615,0,size.height*0.2686367);
    path0.quadraticBezierTo(0,size.height*0.2016454,0,size.height*0.0006716);
    path0.close();

    canvas.drawPath(path0, paint0);


  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }

}

class RPSCustomPainter5 extends CustomPainter{

  @override
  void paint(Canvas canvas, Size size) {



    Paint paint0 = Paint()
      ..color = const Color(0xFF2661FA).withOpacity(0.3)
      ..style = PaintingStyle.fill
      ..strokeWidth = 1;



    Path path0 = Path();
    path0.moveTo(0,0);
    path0.lineTo(size.width,0);
    path0.quadraticBezierTo(size.width*0.9053571,size.height*0.3794493,size.width*0.0017857,size.height*0.2736736);
    path0.quadraticBezierTo(size.width*-0.0071429,size.height*0.1981195,0,0);
    path0.close();




    canvas.drawPath(path0, paint0);


  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }

}

class RPSCustomPainter6 extends CustomPainter{

  @override
  void paint(Canvas canvas, Size size) {



    Paint paint0 = Paint()
      ..color = const Color(0xFF2661FA).withOpacity(0.4)
      ..style = PaintingStyle.fill
      ..strokeWidth = 1;



    Path path0 = Path();
    path0.moveTo(0,0);
    path0.lineTo(size.width,0);
    path0.quadraticBezierTo(size.width*1.0125000,size.height*0.2056749,size.width,size.height*0.2812290);
    path0.quadraticBezierTo(size.width*0.6196429,size.height*0.3274009,0,0);
    path0.close();

    canvas.drawPath(path0, paint0);


  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }

}
