import 'package:flutter/cupertino.dart';
import 'dart:ui' as ui;

class RPSCustomPainter extends CustomPainter{

  @override
  void paint(Canvas canvas, Size size) {



    Paint paint0 = Paint()
      ..color = const Color.fromARGB(255, 22, 23, 67)
      ..style = PaintingStyle.fill
      ..strokeWidth = 1;


    Path path0 = Path();
    path0.moveTo(0,0);
    path0.lineTo(size.width,0);
    path0.quadraticBezierTo(size.width,size.height*0.3776444,size.width,size.height*0.5035259);
    path0.quadraticBezierTo(size.width*0.9852714,size.height*0.4348959,size.width*0.7625000,size.height*0.4372062);
    path0.lineTo(size.width*0.2514286,size.height*0.4388852);
    path0.quadraticBezierTo(size.width*0.1021429,size.height*0.4430826,0,size.height*0.3700470);
    path0.quadraticBezierTo(0,size.height*0.2775353,0,0);
    path0.close();

    canvas.drawPath(path0, paint0);


  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }

}

