import 'dart:io';

import 'package:betgogo/Home/actualbackcreatebet.dart';
import 'package:betgogo/Home/friendlybet.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'dart:convert';

import '../model/storedid.dart';
import 'home.dart';


class create_new_bet extends StatefulWidget {
  const create_new_bet({Key? key}) : super(key: key);

  @override
  State<create_new_bet> createState() => _create_new_betState();
}

class _create_new_betState extends State<create_new_bet> {

  TextEditingController price = TextEditingController();


  Friendlybet bet = Friendlybet("", DateTime.now(), "", 0, 0) ;




  var url = Uri.parse('http://10.0.2.2:8000/api/friendlybet');


  Future save(int id) async {
    print('toto');
    var res = await http.post(url,
        headers: {'Content-Type': 'application/json; charset=UTF-8',},
        body: json.encode({

          'email': bet.email,
          'participant': id,
          'price': bet.price,
          'description': bet.description,
          'date' : bet.date.toString()

        })
    );

    if(res.statusCode==200) {
      Map info = jsonDecode(res.body) as Map;
      String msg = info['msg'];
      AlertDialog alert = AlertDialog(

        content: Text(
          msg,
          style: TextStyle(

              fontSize: 25
          ),
          textAlign: TextAlign.center,
        ),

        actions: [
          Lottie.asset('lotties/fail.json'),

          TextButton(
            child: Center(child: Text("Cancel",
            style: TextStyle(
                color: Color(0xFF2661FA)
            ),)),
            onPressed:  () {
              Navigator.of(context).pop();
            },
          ),

        ],


      );


      AlertDialog alert2 = AlertDialog(

        content: Text(
          msg,
          style: TextStyle(

              fontSize: 25
          ),
          textAlign: TextAlign.center,
        ),

        actions: [
          Lottie.asset('lotties/96237-success.json'),

          TextButton(
            child: Center(child: Text("Go to my bets",
            style: TextStyle(
              color: Color(0xFF2661FA)
            ),),),
            onPressed:  () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => home(i : 1,)));
            },
          ),



        ],



      );
      if(msg == 'User not found'){
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return alert;
          },
        );
      }else {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return alert2;
          },
        );



      }

    }
  }
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(

      body: SingleChildScrollView(
        child: actualbackcbet(
          child: SingleChildScrollView(
            child: Container(
              width: size.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: size.height/10),
                  Text('Create your bet here ! ',
                      style : TextStyle(
                          color : Colors.white,
                          fontSize: 25,
                          //fontWeight: FontWeight.bold
                          fontFamily: 'Proxima',
                          letterSpacing: 1.3
                      )),
                  SizedBox(height: size.height/13),

                  Center(
                    child: CircleAvatar(
                      backgroundImage: AssetImage('Assets/betlogo.jpg'),
                      radius: 50,
                    ),
                  ),
                  SizedBox(height: 20,),
                  Container(
                    width: size.width-60,
                    child: TextFormField(
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color : Colors.transparent),
                          borderRadius: BorderRadius.circular(5.5),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color : Colors.transparent),
                          borderRadius: BorderRadius.circular(5.5),
                        ),
                        prefixIcon: Icon(
                          Icons.person,
                          color: Color(0xFF2661FA),
                        ),
                        hintText : "Opponent Email",
                        hintStyle :  TextStyle(color :  Colors.black),
                        filled :  true,
                        fillColor : Color(0xFF2661FA).withOpacity(0.2),
                      ),
                      controller: TextEditingController(text: bet.email),
                      onChanged: (val){
                        bet.email = val;
                      },
                    ),
                  ),

                  SizedBox(height: 20,),



                  Container(
                    alignment: Alignment.topLeft,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(30, 0, 0, 0),
                      child: Text('Bet Description :',
                        style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.w400,

                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 8,),

                  Container(
                    width: size.width-60,
                    child: TextFormField(
                      keyboardType: TextInputType.multiline,
                      maxLines: 5,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color : Colors.transparent),
                          borderRadius: BorderRadius.circular(5.5),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color : Colors.transparent),
                          borderRadius: BorderRadius.circular(5.5),
                        ),
                        /*prefixIcon: Icon(
                          Icons.person,
                          color: Color(0xFF2661FA),
                        ), */
                        // hintText : "Description",

                        hintStyle :  TextStyle(color :  Colors.black),
                        filled :  true,
                        fillColor : Color(0xFF2661FA).withOpacity(0.2),
                      ),
                      controller: TextEditingController(text: bet.description),
                      onChanged: (val){
                        bet.description = val;
                      },
                    ),
                  ),
                  SizedBox(height: 20,),

                  Container(
                    width: size.width-60,
                    child: TextFormField(
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color : Colors.transparent),
                          borderRadius: BorderRadius.circular(5.5),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color : Colors.transparent),
                          borderRadius: BorderRadius.circular(5.5),
                        ),
                        prefixIcon: Icon(
                          Icons.money_sharp,
                          color: Color(0xFF2661FA),
                        ),
                        hintText : "Amount ",
                        hintStyle :  TextStyle(color :  Colors.black),
                        filled :  true,
                        fillColor : Color(0xFF2661FA).withOpacity(0.2),
                      ),
                      keyboardType: TextInputType.number,
                      //controller: TextEditingController(text: bett.price.toString()),
                      controller: price,
                      onChanged: (val){
                        bet.price = int.parse(price.text);
                      },
                    ),
                  ),
                  SizedBox(height: 20,),

                  Container(
                    height: 50,
                    width: 150,
                    child: ElevatedButton(onPressed: () {
                      _selectDate(context);
                    },
                      style: ElevatedButton.styleFrom(
                        primary:  Color(0xFF2661FA),
                      ),
                      child: Text('Select Date'),

                    ),
                  ),

                  SizedBox(height: 10,),
/*
                  Container(
                   // alignment: Alignment.centerRight,
                    margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                    child: RaisedButton(
                      onPressed: () async {





                      },
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
                      textColor: Colors.white,
                      padding: const EdgeInsets.all(0),
                      child: Container(
                        alignment: Alignment.center,
                        height: 50.0,
                        width: size.width * 0.5,
                        decoration: new BoxDecoration(
                            borderRadius: BorderRadius.circular(80.0),
                            gradient: new LinearGradient(
                                colors: [
                                  Color(0xFF963096),
                                  Color(0xFF2661FA),
                                ]
                            )
                        ),
                        padding: const EdgeInsets.all(0),
                        child: Text(
                          "Submit",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                    ),
                  ),
                  */

                ],
              ),
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(

        onPressed: (){
          print(bet.date);
          save(Provider.of<ID>(context,listen : false ).getid());

        },
        child: Icon(Icons.check),
        backgroundColor:Color(0xFF2661FA) ,



      ),

    );
  }
  _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(), // Refer step 1
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != bet.date)
      setState(() {
        bet.date = picked;
      });
  }
}
