import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: size.height/15,),
          Padding(
            padding: const EdgeInsets.fromLTRB(0,10,10,10),
            child: Container(
              alignment: Alignment.topRight,
              child: IconButton(
                icon : Icon(CupertinoIcons.moon_stars,),
                onPressed: () {  },
              ),
            ),
          ),
         Padding(
           padding: const EdgeInsets.all(20.0),
           child: CircleAvatar(
             radius: 85,
             backgroundColor: Color(0xFF963096),
             child: CircleAvatar(

               backgroundImage: AssetImage('Assets/profile2.jpg'),
               radius: 80,

             ),
           ),
         ),
          Text('Sami Ribardiere',
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text('Sami.ribardiere@gmail.com'),
          ),

          Text('20',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),

        ],
      ),
    );
  }
}
