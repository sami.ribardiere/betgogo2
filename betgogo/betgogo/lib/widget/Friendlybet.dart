import 'dart:ui';

import 'package:betgogo/Home/updateform.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import '../Home/home.dart';
import '../bet.dart';
import 'dart:convert';

import '../model/storedid.dart';

class friendlybet extends StatefulWidget {

  FBetDisplay mybet;


  friendlybet({Key? key, required this.mybet}) : super(key: key);

  @override
  State<friendlybet> createState() => _friendlybetState();
}

class _friendlybetState extends State<friendlybet> {

int temp_winner = 0;
int fbet = 0;

  var url = Uri.parse('http://10.0.2.2:8000/api/FriendlyBetWinnerRequest');

  Future save(int id) async {
    var res = await http.post(url,
        headers: {'Content-Type': 'application/json; charset=UTF-8',},
        body: json.encode({
          'user_id': id,
          'temp_winner' : temp_winner,
          'fbet_id' : fbet,

        })
    );
  }
  String answer = "";
  int bet_id = 0;
var url2 = Uri.parse('http://10.0.2.2:8000/api/FriendlyBetWinnerResponse');
  Future validate_winner() async {
    var res = await http.post(url2,
        headers: {'Content-Type': 'application/json; charset=UTF-8',},
        body: json.encode({

          'answer' : answer,
          'bet_id' : widget.mybet.id

        })
    );
  }



  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    var padtop = size.height/100;
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        children: [

          SizedBox(height: padtop,),
          Container(
            height: size.height/2.3,

            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('Assets/backcrypto.jpg'),
                    fit: BoxFit.cover

                ),

                borderRadius: BorderRadius.all(Radius.circular(20))
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
                child: Container(
                  //color: Colors.white.withOpacity(0.1),
                  child: Column(
                    children: [

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [

                          Column(
                            children: [
                              SizedBox(height: padtop*3,),
                              Padding(
                              padding: const EdgeInsets.fromLTRB(50, 0, 0, 10),
                              child: CircleAvatar(
                                backgroundImage: MemoryImage(widget.mybet.p1image_memory),
                                radius: 35,
                              ),
                            ),

                              Padding(
                                padding: const EdgeInsets.fromLTRB(50, 0, 0, 20),
                                child: Text( widget.mybet.name1,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,

                                  ),
                                ),
                              ),


                            ],
                          ),
                          Container(
                            color: Colors.white,
                            width: 2,
                            height: 120,

                          ),

                          Column(
                            children: [
                              SizedBox(height: padtop*3,),
                              Padding(
                                padding:const EdgeInsets.fromLTRB(0, 0, 50, 10),
                                child: CircleAvatar(
                                  backgroundImage: MemoryImage(widget.mybet.p2image_memory),
                                  radius: 35,
                                ),

                              ),

                              Padding(
                                padding: const EdgeInsets.fromLTRB(0, 0, 50, 20),
                                child: Text(widget.mybet.name2,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,

                                  ),
                                ),
                              ),
                            ],
                          ),



                        ],
                      ),

                      SizedBox(height: padtop*2,),

                      Padding(
                        padding: const EdgeInsets.fromLTRB(30,0,30,10),
                        child: Center(
                          child: Text("Description : " + widget.mybet.description,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,

                            ),),
                        ),
                      ),
                      SizedBox(height: padtop,),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(30,0,30,0),
                        child: Center(
                          child: Text("Amount : " + widget.mybet.price.toString() + "£",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,

                            ),),
                        ),
                      ),
                      SizedBox(height: padtop*1.5,),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(30,0,30,10),
                        child: Center(
                          child: Text("Duedate : " + widget.mybet.date,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,

                            ),),
                        ),
                      ),

                      if (widget.mybet.state == "On_Going") ...[
                        SizedBox(height: padtop*2,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,

                          children: [

                            Padding(
                              padding: const EdgeInsets.fromLTRB(25,0,0,0),
                              child: Container(
                                alignment: Alignment.centerRight,

                                child: RaisedButton(
                                  onPressed: () {
                                    temp_winner = widget.mybet.p1_id;
                                    fbet = widget.mybet.id;
                                    save(Provider.of<ID>(context, listen: false).getid());
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => home(i : 1)));

                                  },
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
                                  textColor: Colors.white,
                                  padding: const EdgeInsets.all(0),
                                  child: Container(
                                    alignment: Alignment.center,
                                    height: 40,
                                    width: size.width/3,
                                    decoration: new BoxDecoration(
                                        borderRadius: BorderRadius.circular(80.0),
                                        gradient: new LinearGradient(
                                            colors: [
                                              Color(0xFF963096),
                                              Color(0xFF2661FA),
                                            ]
                                        )
                                    ),
                                    padding: const EdgeInsets.all(0),
                                    child: Text(
                                      widget.mybet.name1 + ' won',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 0, 25, 0),
                              child: Container(
                                alignment: Alignment.centerRight,

                                child: RaisedButton(
                                  onPressed: () {
                                    temp_winner = widget.mybet.p2_id;
                                    fbet = widget.mybet.id;
                                    save(Provider.of<ID>(context, listen: false).getid());
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => home(i : 1)));

                                  },
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
                                  textColor: Colors.white,
                                  padding: const EdgeInsets.all(0),
                                  child: Container(
                                    alignment: Alignment.center,
                                    height: 40,
                                    width: size.width/3,
                                    decoration: new BoxDecoration(
                                        borderRadius: BorderRadius.circular(80.0),
                                        gradient: new LinearGradient(
                                            colors: [
                                              Color(0xFF963096),
                                              Color(0xFF2661FA),
                                            ]
                                        )
                                    ),
                                    padding: const EdgeInsets.all(0),
                                    child: Text(
                                      widget.mybet.name2 + ' won',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        )
                      ] else if(widget.mybet.state == "Pending" && widget.mybet.bet_winner_sender_id == Provider.of<ID>(context).getid()) ...[
                        Text('Waiting for opponent validation ...',
                        style: TextStyle(
                          color: Colors.red
                        ),)
                      ] else if (widget.mybet.state == "Pending" && widget.mybet.bet_winner_sender_id != Provider.of<ID>(context).getid()) ...[
                        if(widget.mybet.fbet_winner_request_id == widget.mybet.p1_id) ...[
                          Column(
                            children: [
                              Text('did ' + widget.mybet.name1 + ' won ?',
                              style: TextStyle(color: Colors.green,
                                fontSize: 16,),),
                              Center(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(25,2,0,0),
                                      child: ElevatedButton(onPressed: () {
                                        answer = "yes";
                                        validate_winner();
                                        Navigator.push(context, MaterialPageRoute(builder: (context) => home(i : 1)));
                                        print('toto');


                                      },
                                          style: ElevatedButton.styleFrom(
                                              primary: Color(0xFF963096)
                                          ),child: Text('Yes !')),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(0, 2, 25, 0),
                                      child: ElevatedButton(onPressed: () {
                                        answer = "no";
                                        validate_winner();
                                        Navigator.push(context, MaterialPageRoute(builder: (context) => home(i : 1)));


                                      }, style: ElevatedButton.styleFrom(
                                        primary: Color(0xFF963096)
                                      ),
                                          child: Text('No !')),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          )


                        ] else ...[
                          Column(
                            children: [
                              Text('did ' + widget.mybet.name2 + ' won ?',
                              style: TextStyle(
                                color: Colors.green,
                                fontSize: 16,
                              ),),
                              Center(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(25,2,0,0),
                                      child: ElevatedButton(onPressed: () {
                                        answer = "yes";
                                        validate_winner();
                                        Navigator.push(context, MaterialPageRoute(builder: (context) => home(i : 1)));

                                      },style: ElevatedButton.styleFrom(
                                          primary: Color(0xFF963096)
                                      ), child: Text('Yes !')),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(0, 2, 25, 0),
                                      child: ElevatedButton(onPressed: () {
                                        answer = "no";
                                        validate_winner();
                                        Navigator.push(context, MaterialPageRoute(builder: (context) => home(i : 1)));

                                      },style: ElevatedButton.styleFrom(
                                          primary: Color(0xFF963096)
                                      ), child: Text('No !')),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          )
                        ]

                      ]


                      //SizedBox(height: size.height/37,),




                    ],

                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }



}
