import 'dart:ui';

import 'package:betgogo/Home/updateform.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:lottie/lottie.dart';
import '../Home/home.dart';
import '../bet.dart';
import 'dart:convert';

class pendingbetrequest extends StatefulWidget {

  FBet mybet;


  pendingbetrequest({Key? key, required this.mybet}) : super(key: key);

  @override
  State<pendingbetrequest> createState() => _pendingbetrequestState();
}

class _pendingbetrequestState extends State<pendingbetrequest> {



  String answer = "";
  var url = Uri.parse('http://10.0.2.2:8000/api/AcceptBetRequest');

  Future save() async {

    var res = await http.post(url,
        headers: {'Content-Type': 'application/json; charset=UTF-8',},
        body: json.encode({
          "bet_request_id" : widget.mybet.id,
          'answer': answer
        })
    );

  }







  @override
  Widget build(BuildContext context) {
    int deslength = (widget.mybet.description).length;
    int add = 0;
    String backslash = '\n';

  

    
    if ((widget.mybet.description).contains('\n')){
      add += 0;
    }

    Size size = MediaQuery.of(context).size;
    var padtop = size.height/100;
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        children: [
          Text('Bet Request Pending',
            style: TextStyle(
                fontSize: 25
            ),),
          SizedBox(height: 10,),
          Container(
            height: size.height/2.5 + deslength/2 + add,

            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('Assets/backcrypto.jpg'),
                    fit: BoxFit.cover

                ),

                borderRadius: BorderRadius.all(Radius.circular(20))
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
                child: Container(
                  //color: Colors.white.withOpacity(0.1),
                  child: Column(
                    children: [

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            children: [ Padding(
                              padding: const EdgeInsets.fromLTRB(50, 20, 0, 10),
                              child: CircleAvatar(
                                backgroundImage: MemoryImage(widget.mybet.p1image_memory),
                                radius: 35,
                              ),
                            ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(50, 10, 0, 20),
                                child: Text( widget.mybet.name1,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,

                                  ),
                                ),
                              ),


                            ],
                          ),
                          Container(
                            color: Colors.white,
                            width: 2,
                            height: 120,

                          ),

                          Column(
                            children: [
                              SizedBox(height: padtop,),
                              Padding(
                                padding:const EdgeInsets.fromLTRB(0, 0, 50, 10),
                                child: CircleAvatar(
                                  backgroundImage: MemoryImage(widget.mybet.p2image_memory),
                                  radius: 35,
                                ),

                              ),
                              SizedBox(height: padtop,),

                              Padding(
                                padding: const EdgeInsets.fromLTRB(0, 0, 50, 20),
                                child: Text(widget.mybet.name2,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,

                                  ),
                                ),
                              ),
                            ],
                          ),



                        ],
                      ),

                      SizedBox(height: padtop,),

                      Padding(
                        padding: const EdgeInsets.fromLTRB(30,0,30,10),
                        child: Center(
                          child: Text("Description : " + widget.mybet.description,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,

                            ),),
                        ),
                      ),

                      SizedBox(height: padtop,),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(30,0,30,0),
                        child: Center(
                          child: Text("Amount : " + widget.mybet.price.toString() + "£",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,

                            ),),
                        ),
                      ),
                      SizedBox(height: padtop,),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(30,0,30,10),
                        child: Center(
                          child: Text("Duedate : " + widget.mybet.date,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,

                            ),),
                        ),
                      ),
                      SizedBox(height: padtop,),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [

                          Padding(
                            padding: const EdgeInsets.fromLTRB(15,0,0,0),

                            child: CircleAvatar(
                              radius: 25,
                              backgroundColor: Color(0xFF963096),
                              child: IconButton(onPressed: (){
                                answer = 'no';
                                save();
                                Navigator.push(context, MaterialPageRoute(builder: (context) => home(i:3)));



                              },
                                icon: Icon(Icons.delete),
                                color: Colors.black,
                              ),
                            ),
                          ),

                          Padding(
                            padding: const EdgeInsets.fromLTRB(0,0,15,0),
                            child: CircleAvatar(
                              radius: 25,
                              backgroundColor: Color(0xFF963096),
                              child: IconButton(onPressed: (){
                                answer = 'yes';
                                save();
                                Navigator.push(context, MaterialPageRoute(builder: (context) => home(i:3)));

                              },
                                icon: Icon(Icons.check),
                                color: Colors.black,
                              ),
                            ),
                          ),
                          //SizedBox(height: size.height/37,),




                        ],

                      ),

                      //SizedBox(height: size.height/37,),




                    ],

                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(



          content: Column(
            children: [
              Lottie.asset('lotties/111072-thinking-man.json'),
              Text(
                'Are you sure you want to delete this bet ?',
                textAlign: TextAlign.center,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(onPressed: (){
                    Navigator.of(context).pop();
                  }, child: Text('No')),
                  ElevatedButton(onPressed: (){
                    save();
                    Navigator.of(context).pop();
                  }, child: Text('Yes')),
                ],
              )
            ],
          ),

        );
      },
    );
  }
}
