from rest_framework import serializers
from .models import User, Bet, FriendlyBet, SignUpToken, FriendlyBetRequest, FriendlyBetWinnerRequest
from drf_extra_fields.fields import Base64ImageField
import base64


class UserSerializer(serializers.ModelSerializer):
    image_memory=serializers.SerializerMethodField("get_image_memory")
    class Meta:
        model = User
        fields = ['id', 'name', 'username', 'email', 'password','profilepic','image_memory','betwon','betplayed']
        extra_kwargs = {
            'password' : {'write_only': True}
        }
    def get_image_memory(request,bet:Bet):
            with open("media/"+bet.profilepic.name, 'rb') as loadedfile:
                return base64.b64encode(loadedfile.read())
            return  

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance


class BetSerializer(serializers.ModelSerializer):
    image_memory=serializers.SerializerMethodField("get_image_memory")
    class Meta:
        model = Bet
        fields = ['id','name1','name2','price','description','user','date','profilepic','image_memory']

    def get_image_memory(request,bet:Bet):
            with open("media/"+bet.profilepic.name, 'rb') as loadedfile:
                return base64.b64encode(loadedfile.read())
            return  

class FriendlyBetSerializer(serializers.ModelSerializer):
    participant = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), many = True)
    people = serializers.SerializerMethodField('get_people')
    state = serializers.SerializerMethodField('get_state')
    
    bet_appected = serializers.BooleanField(read_only = True)
    is_pending = serializers.BooleanField(read_only = True)
    is_finished = serializers.BooleanField(read_only = True)
    class Meta:
        model = FriendlyBet
        fields = ['id', 'participant', 'email', 'price', 'description', 'date', 'people','bet_appected','is_pending','is_finished','state']
        read_only_fields = ('people','bet_appected')
        

    def get_people(self, FriendlyBet):
       
        serializer = UserSerializer(FriendlyBet.participant,many=True)
        
        return serializer.data 

    def get_state(self,FriendlyBet):
        if (FriendlyBet.is_pending==True):
            friendly_Bet_Winner_Request = FriendlyBetWinnerRequest.objects.filter(friendlybet_id=FriendlyBet.id).first()
            s = FriendlyBetWinnerRequestSerializer(friendly_Bet_Winner_Request)
            state = {'state' : 'Pending' ,
                    'info' : s.data} 
        if (FriendlyBet.is_finished==True):
            state = {'state' : 'Finished',}
        if (FriendlyBet.is_pending==False and FriendlyBet.is_finished==False):
            state = {'state' :'On_Going'}
        return state 



class SignUpTokenSerializer(serializers.ModelSerializer):

    class Meta:
        model = SignUpToken
        fields = ['id', 'timestamp','user','code']

class BetRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = FriendlyBetRequest
        fields = ['id', 'from_user', 'to_user', 'friendlybet_id']

class FriendlyBetWinnerRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = FriendlyBetWinnerRequest
        fields = ['id', 'bet_winner_sender', 'winner_request', 'friendlybet_id']

