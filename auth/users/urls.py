
from django.urls import path
from .views import RegisterView, LoginView, BetView, BetDisplayView, FriendlyBetView,FriendlyBetDisplayView, ProfileView, ChangePasswordView, VerifyEmail, EmailPageView, FriendlyBetWinnerRequestView,  BetRequestView, AcceptBetRequestView, DisplayBetRequestView, ForgotPassword, ResetPasswordView, FriendlyBetWinnerResponseView

urlpatterns = [
  path('register', RegisterView.as_view()),
  path('login', LoginView.as_view()),
  path('bet',BetView.as_view()),
  path('betdisplay',BetDisplayView.as_view()),
  path('friendlybet', FriendlyBetView.as_view()),
  path('friendlybetDisplay', FriendlyBetDisplayView.as_view()),
  path('Profile', ProfileView.as_view()),
  path('ChangePassword', ChangePasswordView.as_view()),
  path('email_verify', VerifyEmail.as_view(), name="email_verify"),
  path('emailPage', EmailPageView.as_view()),
  path('FriendlyBetWinnerRequest', FriendlyBetWinnerRequestView.as_view()),
  path('BetRequest',  BetRequestView.as_view()),
  path('AcceptBetRequest',  AcceptBetRequestView.as_view()),
  path('DisplayBetRequest',  DisplayBetRequestView.as_view()),
  path('forgot_password', ForgotPassword.as_view(), name="forgot_password"),
  path('ResetPassword',  ResetPasswordView.as_view()),
  path('FriendlyBetWinnerResponse',  FriendlyBetWinnerResponseView.as_view()),

]
