# Generated by Django 4.0.6 on 2022-07-14 13:17

from django.db import migrations, models
import users.models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0007_alter_bet_profilepic'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bet',
            name='profilepic',
            field=models.ImageField(default='http://127.0.0.1:8000/media/images/default.png', null=True, upload_to=users.models.user_directory_path),
        ),
    ]
