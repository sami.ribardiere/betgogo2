# Generated by Django 4.0.6 on 2022-07-12 12:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0004_alter_bet_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bet',
            name='date',
            field=models.CharField(max_length=255),
        ),
    ]
