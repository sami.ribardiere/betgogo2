# Generated by Django 4.0.6 on 2022-07-28 13:39

from django.db import migrations, models
import users.models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0013_remove_friendlybet_participant_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='avatar',
            field=models.ImageField(blank=True, default='images/photo2.jpg', null=True, upload_to=users.models.user_directory_path),
        ),
    ]
