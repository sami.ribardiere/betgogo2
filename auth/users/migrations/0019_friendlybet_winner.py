# Generated by Django 4.0.6 on 2022-08-01 10:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0018_user_betplayed_user_betwon'),
    ]

    operations = [
        migrations.AddField(
            model_name='friendlybet',
            name='Winner',
            field=models.IntegerField(default=2),
        ),
    ]
