from django.shortcuts import render
from rest_framework.views import APIView
from .serializers import UserSerializer, BetSerializer, FriendlyBetSerializer, SignUpTokenSerializer, BetRequestSerializer
from rest_framework.response import Response
from rest_framework.exceptions import AuthenticationFailed
from .models import User, Bet, FriendlyBet, SignUpToken, FriendlyBetRequest, FriendlyBetWinnerRequest
import jwt, datetime
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework_simplejwt.tokens import RefreshToken
from .utils import Util
from django.contrib.sites.shortcuts import get_current_site
from django.urls import reverse
from rest_framework import generics
import random


# Create your views here.
class RegisterView(APIView):
    def post(self,request):
       
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid() :
            serializer.save()
            user_data = serializer.data
            user=User.objects.get(email=user_data['email'])
            token=RefreshToken.for_user(user).access_token

            current_site=get_current_site(request).domain

            relativeLink=reverse('email_verify')

            randomnumber = random.randrange(100000, 999999)
            absurl='http://'+current_site+relativeLink+"?token="+str(token)
            email_body='Dear ' + user.username + ',\n' 'Please use the number below in the application to verify your account \n' + str(randomnumber)
            data={'email_body':email_body, 'to_email':user.email, 'email_subject':'Verify your email '}
            Util.send_email(data)
            
            s = SignUpTokenSerializer(data = {
                'user' : user.id,
                'code' : randomnumber
                })
            if s.is_valid() :
               s.save()
                

           
            return Response(
                {
                    'msg' : 'success',
                    'code' : randomnumber,
                    'id' : user.id,
                    'msg3' : s.data,
                    
                    
                }
            )
        return Response(
                {
                    'msg' : 'Username or email are already used'  
                }
            )

class EmailPageView(APIView):
    def post(self,request):
        user_id = request.data['userid']
        user_code = request.data['code']
        user = User.objects.filter(pk=user_id).first()
       
        usertoken = SignUpToken.objects.filter(user=user_id).first()
        if (int(user_code) == usertoken.code):
            user.isemailvalid = True
            user.save()
            usertoken.delete()
            return Response({
                'msg' : 'Email Verified Successfully',
            })

        return Response ({
            'msg' : 'Wrong code'
        })




class VerifyEmail(generics.GenericAPIView):
    def get(self):
        pass



class LoginView(APIView):
     def post(self,request):
        email = request.data['email']
        password = request.data['password']

        user = User.objects.filter(email=email).first()

        if user is None:
            return Response({
            'detail': 0,
            'msg' : 'User not found !',
            'jwt' : 'error'
        })

        if not user.check_password(password):
            return Response({
            'detail':0,
            'msg' : 'Incorrect password !',
            'jwt' : 'error'
        })

        
        payload = {
            'id': user.id,
            'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=60),
            'iat': datetime.datetime.utcnow()
        }

        token = jwt.encode(payload, 'secret',  algorithm='HS256')
        

        response = Response()

        response.set_cookie(key='jwt', value=token, httponly=True)
        response.data = {
                'detail': user.id,
                'msg' : 'success',
                'jwt' : token
            }
       
        if (user.isemailvalid==True):
            return response
        return Response({
            'msg' : 'user did not validate email'
        })
        

class ProfileView(APIView):
    def post(self,request):
        useridd = request.data['userid']
        user = User.objects.filter(pk=useridd).first()
        if user is None:
            return Response({'msg' : 'user does not exist'})
       
        serializer = UserSerializer(user)
        return Response(serializer.data)

    def put(self,request,*args, **kwargs):
        user_id = request.data['userid']
        user = User.objects.filter(pk=user_id)[0]

        data = request.data

        user.username = data["username"]
        user.profilepic = data["profilepic"]

        serializer = UserSerializer(user)
        user.save()

        

        return Response(serializer.data)

class ChangePasswordView(APIView):
    def put(self,request,*args, **kwargs):
        
        user_id = request.data['userid']
        newpassword = request.data['newpassword']
        user = User.objects.filter(pk=user_id).first()
        if user is None:
            raise AuthenticationFailed('User not found!')
            
        

        user.set_password(newpassword)
        
        serializer = UserSerializer(user)
        user.save()

        return Response({'msg' : 'Password Successfully changed'})
    
    def post(self,request):
        email = request.data['email']
        user = User.objects.filter(email=email).first()
        if user is None:
            return Response({'msg' : 'Wrong email',
            'userid' : 0})
        
        

        token=RefreshToken.for_user(user).access_token
        current_site=get_current_site(request).domain

        relativeLink=reverse('forgot_password')

        randomnumber = random.randrange(100000, 999999)
        absurl='http://'+current_site+relativeLink+"?token="+str(token)
        email_body='Hi ' + user.username +' Enter the number below in the application to reset your password\n' + str(randomnumber)
        data={'email_body':email_body, 'to_email':user.email, 'email_subject':'Change your password '}
        Util.send_email(data)
        s = SignUpTokenSerializer(data = {
                'user' : user.id,
                'code' : randomnumber
                })
        if s.is_valid() :
           s.save()
        
        return Response({'msg' : 'Valid Email',
        'userid' : user.id})
    
    
class ResetPasswordView(APIView):
    def post(self,request):
        user_id = request.data['userid']
        user_code = request.data['code']
        user = User.objects.filter(pk=user_id).first()

        usertoken = SignUpToken.objects.filter(user=user_id).last()
        if (int(user_code) == usertoken.code):
            usertoken.delete()
            return Response({
                'msg' : 'code correct',
            })

        return Response ({
            'msg' : 'Wrong code'
        })



   




class ForgotPassword(generics.GenericAPIView):
    def get(self):
        pass





class BetView(APIView):
    def post(self,request):
        serializer = BetSerializer(data=request.data)
        if serializer.is_valid() :
            serializer.save()
            return Response({
                'msg' : serializer.data
            }
            )
        return Response({
        'msg' : 'fail'
    })
    def delete(self,request):
        bet_id = request.data['id']
        bet = Bet.objects.filter(pk=bet_id)
        bet.delete()
        return Response({
            'msg' : 'succesfull'
        })
    
    def get(self,request):
        bets = Bet.objects.all()
        serializer = BetSerializer(bets, many=True)
        return Response(
            serializer.data
        )
    
    def put(self,request,*args, **kwargs):

        bet_id = request.data['id']
        bet = Bet.objects.filter(pk=bet_id)[0]

        data = request.data

        bet.name1 = data["name1"]
        bet.name2 = data["name2"]
        bet.price = data["price"]
        bet.description = data["description"]
        bet.date = data["date"]

        bet.save()

        serializer = BetSerializer(bet)
        return Response(serializer.data)



class BetDisplayView(APIView):
    def post(self,request):
        user_id = request.data['userid']
        bets = Bet.objects.filter(user=user_id)
        serializer = BetSerializer(bets, many=True)
        return Response(
            serializer.data
            )   

class FriendlyBetView(APIView):
    def post(self,request): 
        email = request.data["email"]
        
        p2test = User.objects.filter(email=email).first()
        if p2test is None:
            return Response({'msg' : 'User not found'})
        
        participant2 = User.objects.filter(email=email).first().id
        p2 = User.objects.filter(pk=participant2).first()
        participant = request.data["participant"]
        p1 = User.objects.filter(pk=participant).first()
        price = request.data["price"]
        description =request.data["description"]
        date = request.data["date"]
        p1 =  User.objects.filter(email=email).first()
        s = UserSerializer(p1)
        from_user_id = participant
        to_user_id = participant2
        
        
        serializer = FriendlyBetSerializer(data = {
        "email": email,
        "participant": [
           participant2,
           participant
        ],
        "price": price,
        "description": description,
        "date": date
    })
        if serializer.is_valid() :
            serializer.save()
           
            bet_id =  serializer.data['id']

            serializerr = BetRequestSerializer(data= {
            'from_user' : from_user_id,
            'to_user' : to_user_id,
            'friendlybet_id' : bet_id
            })
            if serializerr.is_valid() :
                serializerr.save() 
                  

                return Response({
                    'msg' : "Request Send ",
                    
                }
                )
        return Response({
        'msg' : 'fail',
        
    })

    def delete(self,request):
         bet_id = request.data['id']
         bet = FriendlyBet.objects.filter(pk=bet_id)
         bet.delete()
         return Response({
            'msg' : 'succesfull'
        })
    def put(self,request,*args, **kwargs):

        bet_id = request.data['id']
        bet = FriendlyBet.objects.filter(pk=bet_id)[0]

        data = request.data
        bet.price = data["price"]
        bet.description = data["description"]
        bet.date = data["date"]

        bet.save()

        serializer = FriendlyBetSerializer(bet)
        return Response(serializer.data)




class FriendlyBetDisplayView(APIView):
    def post(self,request):
        user_id = request.data['userid']
        bets = FriendlyBet.objects.filter(participant=user_id).filter(bet_appected=True)
        
         
        serializer = FriendlyBetSerializer(bets,many=True)
        return Response({"msg" : serializer.data})
        
        
                   

        

        
        


class FriendlyBetWinnerRequestView(APIView):
    def post(self,request):
       temp_winner = request.data['temp_winner']
       user_id = request.data['user_id']
       fbet_id = request.data['fbet_id']
       bet = FriendlyBet.objects.get(pk=fbet_id)
       user = User.objects.get(pk=user_id)
       tempWinner = User.objects.get(pk=temp_winner)
       if bet is None :
            return Response({'msg':'error'})
       FriendlyBetWinnerRequest.objects.create(
        bet_winner_sender = user,
        winner_request = tempWinner,
        friendlybet_id = bet
       )
       bet.is_pending = True
       bet.save()

       return Response({'msg':'success'})
    
    
        

class FriendlyBetWinnerResponseView(APIView):
    def post(self,request):
        answer = request.data['answer']
        fbet_id = request.data['bet_id']
        bet = FriendlyBet.objects.get(pk=fbet_id)
        bet_winner_request = FriendlyBetWinnerRequest.objects.filter(friendlybet_id=fbet_id).last()
        winner = User.objects.get(pk=bet_winner_request.winner_request.id)
        if (str(answer) == 'yes'):
            bet.is_pending = False
            bet.is_finished = True
            bet.bet_appected =  False
            winner.betwon = winner.betwon+1
            winner.save()
            bet.save()
            bet_winner_request.delete()
            return Response({'msg': 'bet finished(yes)'})
        else :
            bet.is_pending = False
            bet.is_finished = True
            bet.bet_appected =  False
            winner.save()
            bet.save()
            bet_winner_request.delete()
            return Response ({'msg': 'bet finished(no)'})

    





        

        
        

class BetRequestView(APIView):
    def post(self,request):
        from_user_id = request.data['from_user_id']
        to_user_email = request.data['to_user_email']
        to_user_id = User.objects.filter(email=to_user_email).first().id
        bet_id = request.data['bet_id']
        serializer = BetRequestSerializer(data= {
            'from_user' : from_user_id,
            'to_user' : to_user_id,
            'friendlybet_id' : bet_id
        })
        if serializer.is_valid() :
            serializer.save() 
            return Response (serializer.data)
    

class AcceptBetRequestView(APIView):
    def post(self,request):
        bet_request_id = request.data['bet_request_id']
        answer = request.data['answer']
        bet_request = FriendlyBetRequest.objects.filter(pk=bet_request_id).first()
        if bet_request is None :
            return Response({"msg" : "error"})
        temp = bet_request.friendlybet_id.id
        friendlybet = FriendlyBet.objects.filter(pk=temp).first()
        p1 = User.objects.get(pk=friendlybet.participant.first().id)
        p2 = User.objects.get(pk=friendlybet.participant.last().id)
        if (str(answer) == 'yes'):
            friendlybet.bet_appected = True
            p1.betplayed = p1.betplayed+1    
            p2.betplayed = p2.betplayed+1
            p1.save()
            p2.save()




         



            friendlybet.save()
            bet_request.delete()
            
        else :
            friendlybet.delete()
            return Response({'msg' : 'deleted'})
        return Response({'msg':'success'})

class DisplayBetRequestView(APIView):
    def post(self,request):
        user_id = request.data['user_id']
        request_pending = FriendlyBetRequest.objects.filter(to_user=user_id)
        test = [t.friendlybet_id.id for t in request_pending]
        bets_pending = FriendlyBet.objects.filter(id__in=test)
        serializer = FriendlyBetSerializer(bets_pending,many=True)
        r_id = [{"id" : r.id} for r in request_pending]
        s = BetRequestSerializer(request_pending, many=True)
        return Response({
            "msg" : serializer.data,
            "bet_request_id" : r_id
         
         }
            )   







        


