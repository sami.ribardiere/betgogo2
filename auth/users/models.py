from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings
from django_resized import ResizedImageField

# Create your models here.

def user_directory_path(instance, filename):
    return 'images/{filename}'.format(filename=filename)

class User(AbstractUser):
    name = models.CharField(max_length=255)
    email = models.CharField(max_length=255,unique= True)
    password = models.CharField(max_length=255)
    username = models.CharField(max_length=255)
    isemailvalid = models.BooleanField(default = False)
    betwon = models.IntegerField(default = 0)
    betplayed = models.IntegerField(default = 0)
    profilepic =  ResizedImageField(size=[300, 300],
        null=True,upload_to=user_directory_path,blank=True, default='images/photo2.jpg'
    )
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

class SignUpToken(models.Model) :
    timestamp = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    code = models.IntegerField()
    

class Bet(models.Model) :
    name1 = models.CharField(max_length=255)
    name2 = models.CharField(max_length=255)
    price = models.IntegerField()
    description =  models.CharField(max_length=255)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.CharField(max_length=255)
    profilepic = models.ImageField(
        null=True,upload_to=user_directory_path,blank=True, default='images/photo2.jpg'
    )
    
    
class FriendlyBet(models.Model):
    participant = models.ManyToManyField(User)
    email =  models.CharField(max_length=255)
    price = models.IntegerField()
    description =  models.CharField(max_length=255)
    date = models.CharField(max_length=255)
    bet_appected = models.BooleanField(default=False)
    is_pending = models.BooleanField(default = False)
    is_finished = models.BooleanField(default = False)

class FriendlyBetWinnerRequest(models.Model):
    bet_winner_sender = models.ForeignKey(User,related_name='bet_winner_sender', on_delete=models.CASCADE)
    winner_request = models.ForeignKey(User,related_name='bet_winner_request', on_delete=models.CASCADE)
    friendlybet_id = models.ForeignKey(FriendlyBet,related_name='bett', on_delete=models.CASCADE)

class FriendlyBetRequest(models.Model):
    from_user = models.ForeignKey(User,related_name='from_user', on_delete=models.CASCADE)
    to_user = models.ForeignKey(User,related_name='to_user', on_delete=models.CASCADE)
    friendlybet_id = models.ForeignKey(FriendlyBet,related_name='bet', on_delete=models.CASCADE)

    
